<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo ($seo_title); ?></title>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<meta name="description" content="<?php echo ($seo_description); ?>"/>
<meta name="keywords" content="<?php echo ($seo_keywords); ?>"/>
<meta name="verify-v1" content="RmwftV8EEKLghfHfzIZsUfuxdr489sZ0CyBlaPgmW6U="/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="Header">
  <div class="topNav"><a href="/">HOME</a> | <a href="sitemap.html">SITEMAP</a> | <a href="contact.html">CONTACT US</a></div>
  <a href="http://www.stanfordmaterials.com/"><img src="images/logo.gif" alt="Stanford Materials" border="0"/></a>
</div>
<div id="Nav">
	<a href="http://www.stanfordmaterials.com/" title="Home">Home</a> |<a href="aboutsm.html">About Us</a> | <a href="product.html">Products</a> | <a href="quote.html">Quotations</a> | <a href="news.html">What's New</a> | <a href="library.html">Reference Library</a>
</div>
<div id="subMain">
            <center>
                <h1>What's New</h1><a name="begin"></a>Last updated on <?php echo ($new_time); ?>
            </center>
            <table border="0" align="center" cellspacing="0" style="width:auto">
                <!--start sub??-->
                <?php if(is_array($sub_news_info)): foreach($sub_news_info as $key=>$vo): ?><tr>
                    <td><a href="#<?php echo ($vo["anchor"]); ?>"><?php echo ($vo["title"]); ?></a></td>
                </tr><?php endforeach; endif; ?>
                <!--end sub??-->
            </table>
    <!--start????-->
    <?php if(is_array($sub_news_info)): foreach($sub_news_info as $key=>$g): ?><p>
                <a name="<?php echo ($g["anchor"]); ?>"></a><b><?php echo ($g["title"]); ?></b>
                <br />
                <?php echo ($g["content"]); ?>
            </p><?php endforeach; endif; ?>
    <!--end????-->



        </div>
<div class="clearAll"></div>
        <div id="Footer">
            &copy; Stanford Materials.      23661 Birtcher Dr, Lake Forest, CA 92630 U.S.A. Tel: (949) 468 0555 Fax: (949) 812-6690    E-mail: <a href="mailto:sales@stanfordmaterials.com">sales@stanfordmaterials.com</a>
        </div>
        <script type="text/javascript">
			var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-11387848-3']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
</body>
</html>
<?php echo ($js_one); ?>
<?php echo ($js_two); ?>
<?php echo ($js_three); ?>