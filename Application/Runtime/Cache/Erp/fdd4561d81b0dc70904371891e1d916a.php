<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="/Public/Erp/Index/css/pintuer.css">
<link rel="stylesheet" href="/Public/Erp/Index/css/admin.css">
<script src="/Public/Erp/Index/js/jquery.js"></script>
<script src="/Public/Erp/Index/js/pintuer.js"></script>
      <link rel="stylesheet" href="/Public/bootstrop/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="/Public/bootstrop/bootstrap.min.js"></script>
</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head"><strong class="icon-reorder"> 添加子类New详情</strong></div>
  <div class="padding border-bottom">
  <button type="button" class="btn btn-primary" onclick="window.location.href='/index.php/Erp/New/addSubNews/newsid/<?php echo ($newsid); ?>'"><span class="icon-plus-square-o"></span> 添加子类New详情</button>
  </div>
  <table class="table table-hover text-center">
    <tr>
      <th>ID</th>
      <th>news_id</th>
      <th>标题</th>
      <th>锚点名称</th>
      <th>排序</th>
      <th>创建人</th>
      <th>更新人</th>
      <th>创建时间</th>
      <th>更新时间</th>
      <th>操作</th>
    </tr>

    <?php if(is_array($data)): foreach($data as $key=>$vo): ?><tr>
      <td><?php echo ($vo["id"]); ?></td>
      <td><?php echo ($vo["news_id"]); ?></td>
      <td><?php echo ($vo["title"]); ?></td>
      <td><?php echo ($vo["anchor"]); ?></td>
      <td><?php echo ($vo["order_status"]); ?></td>
      <td><?php echo ($vo["create_user"]); ?></td>
      <td><?php echo ($vo["update_user"]); ?></td>
      <td><?php echo ($vo["create_time"]); ?></td>
      <td><?php echo ($vo["update_time"]); ?></td>
      <td><div class="button-group">
            <a href="/index.php/Erp/New/showUpdateNews/id/<?php echo ($vo["id"]); ?>" class='btn btn-info'>修改</a>
            <a onclick='delWeb("<?php echo ($vo["id"]); ?>",this)'  class='btn btn-danger'>删除</a>
      </div></td>
    </tr><?php endforeach; endif; ?>


  </table>
</div>
<script type="text/javascript">
function delWeb(id,obj){
    if(confirm("您确定要删除吗?")){
        $(obj).attr('disabled',true);
        $(obj).text('删除中...');
        $.post('/index.php/Erp/New/delSubNews',{id:id},function(v){
            if(v.f){
                alert('删除完成');
                location.reload();
            }else{
                alert(v.data);
                location.reload();
            }
        },'json');
    }
}
</script>
</body></html>