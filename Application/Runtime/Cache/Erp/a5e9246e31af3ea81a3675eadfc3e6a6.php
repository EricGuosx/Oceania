<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="/Public/Erp/Index/css/pintuer.css">
<link rel="stylesheet" href="/Public/Erp/Index/css/admin.css">
<script src="/Public/Erp/Index/js/jquery.js"></script>
<script src="/Public/Erp/Index/js/pintuer.js"></script>
<link rel="stylesheet" href="/Public/bootstrop/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/Public/bootstrop/bootstrap.min.js"></script>
<script src="/vendor/ueditor/ueditor.config.js"></script>
<script src="/vendor/ueditor/ueditor.all.min.js"></script>
<script src="/vendor/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head"><strong><span class="icon-key"></span> 公司简介</strong></div>
  <div class="body-content">
    <form class="form-x" action="">
      <div class="form-group">
        <div class="label">
          <label for="sitename">站点名称：</label>
        </div>
        <div class="field">
          <input type="text" id='website' class="input"   placeholder="" readonly='readonly' value='<?php echo ($info); ?>'/>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">Meta title：</label>
        </div>
        <div class="field">
          <input type="text" id='seoTitle' class="input"   placeholder="" />
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">Meta description：</label>
        </div>
        <div class="field">
          <input type="text" id='seoDescription' class="input"  placeholder=""/>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">Meta keywords：</label>
        </div>
        <div class="field">
           <input type="text" id='seoKeywords' class="input" /><button type="button" class="btn btn-primary btn-xs" onclick="add_reply()">添加</button>
        </div>
                  <div class="label">
          <label for="sitename"></label>
        </div>
        <div class="field">
            <div id='replay_list' class="col-sm-9"></div>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">简介标题：</label>
        </div>
        <div class="field">
          <input type="text" id='title' class="input"  placeholder=""/>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">界面链接：</label>
        </div>
        <div class="field">
          <input type="text" id='static_html' class="input"  placeholder="如填写aboutsm  在网站中将会显示www.xxx.com/aboutsm.html"/>
        </div>
      </div>




      <div class="form-group">
        <div class="label">
          <label for="sitename">统计脚本1：</label>
        </div>
        <div class="field">
         <textarea class="input" placeholder="说明：非必填，请输入谷歌等统计信息脚本" id='js_one'></textarea>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">统计脚本2：</label>
        </div>
        <div class="field">
         <textarea class="input" placeholder="说明：非必填，请输入谷歌等统计信息脚本" id='js_two'></textarea>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">统计脚本3：</label>
        </div>
        <div class="field">
         <textarea class="input" placeholder="说明：非必填，请输入谷歌等统计信息脚本" id='js_three'></textarea>
        </div>
      </div>




      <div class="form-group">
        <div class="label">
          <label for="sitename">简介内容：</label>
        </div>
        <div class="field">
          <textarea id='content'></textarea>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label></label>
        </div>
        <div class="field">
          <button class="button bg-main icon-check-square-o" type="button" onclick="homePageSeo()"> 添加公司简介</button>
        </div>
      </div>
    </form>
  </div>
</div>
</body></html>
<script type="text/javascript">
          Array.prototype.removeByIndex = function (index) {
              if (index > -1) {
                  this.splice(index, 1);
              }
          }
          var ue = UE.getEditor('content',{
              toolbars:[[ 'anchor','link',
                  'unlink', //取消链接
                  '|',
                  'forecolor', //字体颜色
                  'backcolor', //背景色
                  'fontfamily', //字体
                  'fontsize', //字号
                  '|',
                  'bold', //加粗
                  'italic', //斜体
                  'underline', //下划线
                  'strikethrough', //删除线
                  '|',
                  'formatmatch', //格式刷
                  'removeformat', //清除格式
                  '|',
                  'insertorderedlist', //有序列表
                  'insertunorderedlist', //无序列表
                  '|',
                  'inserttable', //插入表格
                  'paragraph', //段落格式
//                  'simpleupload', //单图上传
                  'imagecenter', //居中
//                  'attachment', //附件
                  '|',
                  'justifyleft', //居左对齐
                  'justifycenter', //居中对齐
                  'horizontal', //分隔线
                  '|',
                  'blockquote', //引用
                  'insertcode', //代码语言
                  '|',
                  'source', //源代码
                  'preview', //预览
//                  'fullscreen', //全屏
                   ]],
              //focus时自动清空初始化时的内容
              autoClearinitialContent:true,
              //关闭字数统计
              wordCount:false,
              //关闭elementPath
              elementPathEnabled:false,
              //默认的编辑区域高度
              initialFrameHeight:500
              //更多其他参数，请参考ueditor.config.js中的配置项
          })
          function add_reply() {
              var replay = $('#seoKeywords').val()
              if(!$.trim(replay)){
                  alert('keywords不能为空');
                  return;
              }
              replay_list.push({replay:replay})
              $('#seoKeywords').val('')
              show_replay_list()
          }

          var replay_list = []
          function show_replay_list() {
              $('#remove').remove()
              show_replay = '<div id="remove" class="modelList">'
              for (var i = 0; i < replay_list.length; i++) {
                  show_replay += '<div  style="float:left">'
                  show_replay +=       '<span class="btn btn-info btn-xs" style="margin: 2px">'
                  show_replay +=              replay_list[i].replay
                  show_replay +=         '</span>'
                  show_replay +=     '<a href="javascript:void(0)">'
                  show_replay +=          '<span style="color: red;" onclick=remove_replay_id('+i+')>'
                  show_replay +=             '[-]'
                  show_replay +=          '</span>'
                  show_replay +=       '</a>'
                  show_replay += '</div>'
              }
              show_replay+= '</div>'
              $('#replay_list').append(show_replay)
          }
          function remove_replay_id(id) {
              replay_list.removeByIndex(id)
              show_replay_list()
          }

          function homePageSeo(){
              var website = $('#website').val()
              var seoTitle   = $('#seoTitle').val()
//              alert(seoTitle);
              var seoDescription = $('#seoDescription').val()
              var seoKeywords = replay_list
              var title   = $('#title').val()
              var content = UE.getEditor('content').getContent()
              var static_html = $('#static_html').val()
              var js_one = $("#js_one").val()
              var js_two = $('#js_two').val()
              var js_three = $('#js_three').val()
              if($.trim(website) == ''){
                  alert('站点名称不能为空');return;
              }
              if($.trim(seoTitle) == ''){
                  alert('SEO Title不能为空');return;
              }
              if($.trim(seoDescription) == ''){
                  alert('SEO Description 不能为空');return;
              }
              if($.trim(title) == ''){
                  alert('简介标题不能为空');return;
              }
              if($.trim(content) == ''){
                  alert('简介内容不能为空');return;
              }
              if($.trim(static_html) == ''){
                  alert('界面链接不能为空');return;
              }
              $.post("/index.php/Erp/Homepage/addWebSynopsis",{website:website,seotitle:seoTitle,seodescription:seoDescription,seokeywords:seoKeywords,title:title,content:content,static_html:static_html,js_one:js_one,js_two:js_two,js_three:js_three},function(v){
                  if(v.f){
                      alert(v.data);
                      location.href="/index.php/Erp/Web/info";
                  }else{
                      alert(v.data);
                      location.reload();
                  }
              },'json')
          }
</script>