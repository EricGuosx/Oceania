<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="/Public/Erp/Index/css/pintuer.css">
<link rel="stylesheet" href="/Public/Erp/Index/css/admin.css">
<script src="/Public/Erp/Index/js/jquery.js"></script>
<script src="/Public/Erp/Index/js/pintuer.js"></script>
      <link rel="stylesheet" href="/Public/bootstrop/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="/Public/bootstrop/bootstrap.min.js"></script>
    <style>
                /* start 分页样式 */
                .pages a,.pages span {
                    display:inline-block;
                    padding:2px 5px;
                    margin:0 1px;
                    border:1px solid #f0f0f0;
                    -webkit-border-radius:3px;
                    -moz-border-radius:3px;
                    border-radius:3px;
                }
                .pages a,.pages li {
                    display:inline-block;
                    list-style: none;
                    text-decoration:none; color:#58A0D3;
                }
                .pages a.first,.pages a.prev,.pages a.next,.pages a.end{
                    margin:0;
                }
                .pages a:hover{
                    border-color:#50A8E6;
                }
                .pages span.current{
                    background:#50A8E6;
                    color:#FFF;
                    font-weight:700;
                    border-color:#50A8E6;
                }
                /* end 分页样式 */
    </style>
</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head"><strong class="icon-reorder"> Reference Library</strong></div>
  <div class="padding border-bottom">
  <button type="button" class="button border-yellow" onclick="window.location.href='/index.php/Erp/Library/addLibrary'"><span class="icon-plus-square-o"></span> 添加内容</button>
  </div>
  <table class="table table-hover text-center">
    <tr>
      <!--<th>ID</th>-->
      <th>标题</th>
      <th>界面链接</th>
      <th>SEO Title</th>
      <th style="width: 20%">SEO Description</th>
      <th style="width: 20%">SEO Keywords</th>
      <th>创建人</th>
      <th>更新人</th>
      <th>创建时间</th>
      <th>更新时间</th>
      <th>操作</th>
    </tr>


               <?php if(is_array($info)): foreach($info as $key=>$vo): ?><tr>
      <!--<td><?php echo ($vo["id"]); ?></td>-->
      <td><?php echo ($vo["title"]); ?></td>
      <td><?php echo ($vo["static_html"]); ?></td>
      <td><?php echo ($vo["seotitle"]); ?></td>
      <td><?php echo ($vo["seodescription"]); ?></td>
      <td><?php echo ($vo["seokeywords"]); ?></td>
      <td><?php echo ($vo["create_user"]); ?></td>
      <td><?php echo ($vo["update_user"]); ?></td>
        <td><?php echo ($vo["create_time"]); ?></td>
        <td><?php echo ($vo["update_time"]); ?></td>
      <td><div class="button-group">
            <a href="/index.php/Erp/Library/updateLibrary/id/<?php echo ($vo["id"]); ?>" class='btn btn-info'>修改</a>
             <a  href="/index.php/Erp/Library/subLibrary/id/<?php echo ($vo["id"]); ?>" class='btn btn-warning'>添加Library详情</a>
            <a onclick='delLibrary("<?php echo ($vo["id"]); ?>",this)'  class='btn btn-danger'>删除</a>
      </div></td>
    </tr><?php endforeach; endif; ?>


                  <tr class="content">
                <td colspan="11" bgcolor="#FFFFFF"><div class="pages">
                        <!--<?php echo ($page); ?>-->
                </div></td>
            </tr>
  </table>
    <div class="alert alert-success" role="alert" style="text-align: center;">说明：请在添加完Reference Library的seo与统计代码后，添加对应的Reference Library详情！，如有疑问请联系管理员，ThankYou</div>
</div>
<script type="text/javascript">
function delLibrary(id,obj){
    if(confirm("您确定要删除吗?将会删除Library下的所有内容!")){
        $(obj).attr('disabled',true);
        $(obj).text('删除中...');
        $.post('/index.php/Erp/Library/delLibrary',{id:id},function(v){
            if(v.f){
                alert('删除完成');
                location.href="/index.php/Erp/Library/LibraryList";
            }else{
                alert(v.data);
                location.reload();
            }
        },'json');
    }
}
</script>
</body></html>