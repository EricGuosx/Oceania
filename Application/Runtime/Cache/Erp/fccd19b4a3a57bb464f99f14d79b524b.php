<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>网站信息</title>
    <link rel="stylesheet" href="/Public/Erp/Index/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Erp/Index/css/admin.css">
    <link rel="stylesheet" href="/Public/bootstrop/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="/Public/bootstrop/bootstrap.min.js"></script>
    <script src="/Public/Erp/Index/js/jquery.js"></script>
    <script src="/Public/Erp/Index/js/pintuer.js"></script>

</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head"><strong><span class="icon-pencil-square-o"></span> 网站信息</strong></div>
  <div class="body-content">
        <ul class="list-group ">
            <li class="list-group-item"><label class='label label-info'>登录用户</label>：<?php echo ($info['username']); ?></li>
          <li class="list-group-item"><label class='label label-info'>登陆时间</label>：<?php echo ($info['online_time']); ?></li>
          <li class="list-group-item"><label class='label label-info'>登录  I P</label> ：<?php echo ($info['ip']); ?></li>
          <li class="list-group-item"><label class='label label-info'>当前网站</label>：<?php echo ($info["website"]); ?></li>
          <li class="list-group-item"><label class='label label-info'>登陆次数</label>：<?php echo ($info['count']); ?></li>
        </ul>
      <div class="alert alert-warning" role="alert" style="text-align: center;">说明：请在网站列表选择对应网站后再进行后续操作！！！</div>



  </div>
</div>
</body></html>