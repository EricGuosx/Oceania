<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="/Public/Erp/Index/css/pintuer.css">
<link rel="stylesheet" href="/Public/Erp/Index/css/admin.css">
<script src="/Public/Erp/Index/js/jquery.js"></script>
<script src="/Public/Erp/Index/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head"><strong><span class="icon-key"></span> 添加导航</strong></div>
  <div class="body-content">
    <form class="form-x" action="">
      <div class="form-group">
        <div class="label">
          <label for="sitename">导航名称：</label>
        </div>
        <div class="field">
          <input type="text" id='nav' class="input w50"  size="50" placeholder="" data-validate="required:请输入导航名称" />
        </div>
      </div>

        <div class="form-group">
          <div class="label">
            <label>上级分类：</label>
          </div>
          <div class="field">
            <select name="parentId" id='parentId' class="input w50">
              <option value="0">请选择分类(默认一级分类)</option>
              <?php if(is_array($info)): foreach($info as $k=>$v): ?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["delimiter"]); if($v["parent_id"] != 0): ?>├&nbsp;&nbsp;<?php endif; echo ($v["nav"]); ?></option><?php endforeach; endif; ?>
            </select>
            <div class="tips"></div>
          </div>
        </div>

      <div class="form-group">
        <div class="label">
          <label for="sitename">导航排序：</label>
        </div>
        <div class="field">
          <input type="text" id='order_num' class="input w50"  size="50" placeholder="" data-validate="required:请输入导航排序" />
        </div>
      </div>


      <div class="form-group">
        <div class="label">
          <label></label>
        </div>
        <div class="field">
          <button class="button bg-main icon-check-square-o" type="button" onclick="addNewNav()"> 提交</button>
        </div>
      </div>      
    </form>
  </div>
</div>
</body></html>
<script>
    function addNewNav(){
        var nav = $('#nav').val()
        var order_num = $('#order_num').val()
        if($.trim(nav) == ''){
            alert('请填写导航名称');
            return;
        }
        if($.trim(order_num) ==''){
            alert('请填写导航排序');
            return;
        }
        var menu = $("#parentId").find("option:selected")
//        var txt = menu.text()
        var parent_id = menu.val()
        $.post("/index.php/Erp/Nav/addNewNav",{nav:nav,order_num:order_num,parent_id:parent_id},function(v){
            if(v.f){
                alert(v.data);
                location.href="/index.php/Erp/Nav/navList";
            }else{
                alert(v.data);
                location.reload();
            }
        },'json')
    }
</script>