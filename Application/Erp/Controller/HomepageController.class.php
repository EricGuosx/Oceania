<?php
/**
 * HomepageController.class.php
 * User: Eric
 * Date: 2018/4/4
 * Time: 10:46
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
use Common\Helper\Category;
class HomepageController extends ErpController
{
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * 公司简介
     */
    public function showWebSynopsis()
    {
        $webinfo = $_SESSION['website'];
        if(!$webinfo){
            $result = M('erp_website')->where('status=2')->find();
            $webinfo = $result['website'];
            $_SESSION['website'] = $result['website'];
        }
        $web = $webinfo;
        $this->assign('info',$web);
        $check = M('erp_homepage_synopsis')->where("`website`='$web'")->find();
        if($check){
            $keyword = oneToTwo($check['seokeywords']);
            $keyword3 = json_encode($keyword);
            $this->assign('key',$keyword3);
            $this->assign('seo',$check);
            $this->display('updateWebSynopsis');
        }else{
            $this->display();
        }
    }

    /**
     * 添加公司简介
     */
    public function addWebSynopsis()
    {
        $data = I('post.');
        if(empty($data['title'])){
            returnAjaxJson(false,'请填写文章title');
        }
        if(empty($data['website'])){
            returnAjaxJson(false,'请填写站点名称');
        }
        if(empty($data['seotitle'])){
            returnAjaxJson(false,'请填写SEO Title');
        }
        if(empty($data['seodescription'])){
            returnAjaxJson(false,'请填写SEO Description');
        }
        if(empty($data['content'])){
            returnAjaxJson(false,'请填写内容');
        }
        if(empty($data['static_html'])){
            returnAjaxJson(false,'界面链接不能为空');
        }
        $keywords1 = $data['seokeywords'];
        $count = count($keywords1);
        $keywords2 = twoToOne($keywords1);
        $keywords3 = implode(',',$keywords2);
        if(empty($keywords3) || $count<1){
            returnAjaxJson(false,'请填写SEO Keywords');
        }
        $userinfo =  erpUserInfo();
        $item = [
            'title' => $data['title'],
            'website' => $data['website'],
            'content' => $data['content'],
            'seodescription' => $data['seodescription'],
            'seotitle' => $data['seotitle'],
            'seokeywords' => $keywords3,
            'static_html' => $data['static_html'],
            'create_time' => date('Y-m-d H:i:s',time()),
            'create_user' => $userinfo['username'],
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
            'js_one' => $data['js_one'],
            'js_two' => $data['js_two'],
            'js_three' => $data['js_three'],
        ];
        $id = M('erp_homepage_synopsis')->add($item);
        if($id){
            returnAjaxJson(true,'公司简介添加成功');
        }else{
            returnAjaxJson(false,'系统异常');
        }
    }

    /**
     * 更新公司简介
     */
    public function updateWebSynopsis()
    {
        $data = I('post.');
        $id = $data['id'];
        if(empty($id)){
            returnAjaxJson(false,'缺少对应的 ID，请联系管理员。');
        }
        if(empty($data['title'])){
            returnAjaxJson(false,'请填写文章title');
        }
        if(empty($data['website'])){
            returnAjaxJson(false,'请填写站点名称');
        }
        if(empty($data['seotitle'])){
            returnAjaxJson(false,'请填写SEO Title');
        }
        if(empty($data['seodescription'])){
            returnAjaxJson(false,'请填写SEO Description');
        }
        if(empty($data['content'])){
            returnAjaxJson(false,'请填写内容');
        }
        if(empty($data['static_html'])){
            returnAjaxJson(false,'界面链接不能为空');
        }
        $keywords1 = $data['seokeywords'];
        $count = count($keywords1);
        $keywords2 = twoToOne($keywords1);
        $keywords3 = implode(',',$keywords2);
        if(empty($keywords3) || $count<1){
            returnAjaxJson(false,'请填写SEO Keywords');
        }
        $userinfo =  erpUserInfo();
        $item = [
            'title' => $data['title'],
            'website' => $data['website'],
            'content' => $data['content'],
            'seodescription' => $data['seodescription'],
            'seotitle' => $data['seotitle'],
            'seokeywords' => $keywords3,
            'static_html' => $data['static_html'],
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
            'js_one' => $data['js_one'],
            'js_two' => $data['js_two'],
            'js_three' => $data['js_three'],
        ];
        $id = M('erp_homepage_synopsis')->where("id=$id")->save($item);
        if($id){
            returnAjaxJson(true,'公司简介更新成功');
        }else{
            returnAjaxJson(false,'系统异常');
        }
    }

    /**
     * 首页SEO
     */
    public function homePageSeo()
    {
        $webinfo = $_SESSION['website'];
        if(!$webinfo){
            $result = M('erp_website')->where('status=2')->find();
            $webinfo = $result['website'];
            $_SESSION['website'] = $result['website'];
        }
        $web = $webinfo;
        $this->assign('info',$web);
        $check = M('erp_homepage_seo')->where("`website`='$web'")->find();
        if($check){
            $keyword = oneToTwo($check['keywords']);
            $keyword3 = json_encode($keyword);
            $this->assign('key',$keyword3);
            //dump($keyword3);
            $this->assign('seo',$check);
            $this->display('updateHomePageSeo');
        }else{
            $this->display();
        }
    }

    /**
     * 添加首页SEO 信息
     */
    public function addHomePageSeo()
    {
        $data = I('post.');
        $keywords1 = $data['keywords'];
        $count = count($keywords1);
        $keywords2 = twoToOne($keywords1);
        $keywords3 = implode(',',$keywords2);
        if(empty($keywords3) || $count<1){
            returnAjaxJson(false,'keyword不能为空!!!');
        }
        if(empty($data['title'])){
            returnAjaxJson(false,'title不能为空');
        }
        if(empty($data['description'])){
            returnAjaxJson(false,'description不能为空');
        }
        if(empty($data['website'])){
            returnAjaxJson(false,'website不能为空');
        }
        $userinfo =  erpUserInfo();
        $item = [
            'website'     => $data['website'],
            'title'       => $data['title'],
            'description' => $data['description'],
            'keywords'    => $keywords3,
            'create_time' => date('Y-m-d H:i:s',time()),
            'create_user' => $userinfo['username'],
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
            'js_one' => $data['js_one'],
            'js_two' => $data['js_two'],
            'js_three' => $data['js_three'],
        ];
        $id = M('erp_homepage_seo')->add($item);
        if($id){
             returnAjaxJson(true,'SEO添加成功');
        }else{
             returnAjaxJson(false,'系统异常');
        }
    }





    /**
     * 修改首页SEO 信息
     */
    public function updateHomePageSeo()
    {
        $data = I('post.');
        $id = $data['seoId'];
        if(empty($id)){
            returnAjaxJson(false,'系统未发现对应的ID，请仔细检查');
        }
        $keywords1 = $data['keywords'];
        $count = count($keywords1);
        $keywords2 = twoToOne($keywords1);
        $keywords3 = implode(',',$keywords2);
        if(empty($keywords3) || $count<1){
            returnAjaxJson(false,'keyword不能为空!!!');
        }
        if(empty($data['title'])){
            returnAjaxJson(false,'title不能为空');
        }
        if(empty($data['description'])){
            returnAjaxJson(false,'description不能为空');
        }
        if(empty($data['website'])){
            returnAjaxJson(false,'website不能为空');
        }
        $userinfo =  erpUserInfo();
        $item = [
            'website'     => $data['website'],
            'title'       => $data['title'],
            'description' => $data['description'],
            'keywords'    => $keywords3,
            'update_user' => $userinfo['username'],
            'update_time' => date('Y-m-d H:i:s',time()),
            'js_one' => $data['js_one'],
            'js_two' => $data['js_two'],
            'js_three' => $data['js_three'],
        ];
        $id = M('erp_homepage_seo')->where("id=$id")->save($item);
        if($id){
            returnAjaxJson(true,'SEO更新成功');
        }else{
            returnAjaxJson(false,'系统异常');
        }
    }




}