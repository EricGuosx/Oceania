<?php
/**
 * UserController.class.php
 * User: Eric
 * Date: 2018/3/19
 * Time: 9:29
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
class UserController extends ErpController
{
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * 用户列表
     */
    public function userList()
    {
        $m = M('erp_admin');
        $count = $m->count();
        $p = getpage($count,10);
        $list = M('erp_admin')->where('`status`=1')->field(true)->order('id DESC')->limit($p->firstRow, $p->listRows)->select();
        $this->assign('info', $list);
        $this->assign('page', $p->show());
        $this->display();
    }

    /**
     * 添加用户
     */
    public function userAdd()
    {
        $this->display();
    }

    /**
     * ajax添加用户
     */
    public function addNewUser()
    {
        $data = I('post.');
        if(empty($data['username'])){
            echo returnAjaxJson(false,'请填写用户名');
        }
        if(empty($data['password'])){
            echo returnAjaxJson(false,'密码不能为空');
        }
        if(empty($data['password2'])){
            echo returnAjaxJson(false,'请再次输入密码');
        }
        if($data['password'] != $data['password2']){
            echo returnAjaxJson(false,'两次密码必须保持一致');
        }
        $userItem = [
            'username' => $data['username'],
            'password' => md5($data['password'] . 'Oceania'),
            'create_time' => date('Y-m-d H:i:s',time())
        ];
        $id = M('erp_admin')->add($userItem);
        if($id){
            echo returnAjaxJson(true,'添加成功');
        }else{
            echo returnAjaxJson(false,'发生意料之外的错误');
        }
    }

    /**
     * 更新用户信息
     */
    public function userUpdate()
    {
        $this->assign('username',$_SESSION['username']);
        $this->display();
    }

    /**
     * 更新密码
     */
    public function saveUpdateData()
    {
        $data = I('post.');
        if(empty($data['oldpasswprd'])){
            returnAjaxJson(false,'请填写原始密码');
        }
        if(empty($data['password'])){
            returnAjaxJson(false,'密码不能为空');
        }
        if(empty($data['password2'])){
            returnAjaxJson(false,'请再次输入密码');
        }
        if($data['password'] != $data['password2']){
             returnAjaxJson(false,'两次密码必须保持一致');
        }
        $id = $data['userid'];
        if(!$id){
            $id= $_SESSION['userid'];
        }
        $db_password = M('erp_admin')->where("id=$id")->getField('password');
        $input_pwd = md5($data['oldpasswprd'] . 'Oceania');
        if($db_password != $input_pwd){
            returnAjaxJson(false,'原始密码错误');
        }
        $new_password = md5($data['password'] . 'Oceania');
        if($new_password == $db_password){
            returnAjaxJson(false,'新密码不能与原始密码相同');
        }
        $userItem = [
            'password' =>$new_password,
        ];
        $result = M('erp_admin')->where("id = $id")->save($userItem);
        if($result){
            returnAjaxJson(true,'更新成功');
        }else{
            returnAjaxJson(false,'发生意料之外的错误');
        }
    }

    /**
     * 展示修改用户详情
     */
    public function updateUserDetail()
    {
        $id = I('get.id');
        if(!$id){
            echo '这是一个美丽的错误，请联系管理员';
            die;
        }
        $info = M('erp_admin')->find($id);
        //dump($info);
        $this->assign('info',$info);
        $this->display();
    }

    /**
     * 删除用户
     */
    public function delUser()
    {
        $id = I('post.id');
        if(!$id){
            returnAjaxJson(false,'发生了意料之外的错误，请联系管理员');
        }
        $item = [
            'status' => 2,
        ];
        $result = M('erp_admin')->where("id = $id")->save($item);
        if($result){
            returnAjaxJson(true,'更新成功');
        }else{
            returnAjaxJson(false,'发生意料之外的错误');
        }
    }



}