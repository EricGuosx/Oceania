<?php
/**
 * StanfordmaterialsController.class.php
 * User: Eric
 * Date: 2018/3/29
 * Time: 15:04
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
class StanfordmaterialsController extends ErpController
{
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * 这是一个美丽的测试
     */
    public function test()
    {
        $this->assign('test','一个美丽的测试');
        $this->assign('test2',"alert('ok');");
        $this->assign('test3',"<script>
    alert('test3ok');
</script>");
        // 文件名 文件存放路径  模版文件的位置
        $this->buildHtml("test_layout",HTML_PATH . '/www.stanfordmaterials.com/',"Stanfordmaterials:asd");
        $this->display('asd');
    }

    /**
     * 公司简介
     */
    public function webSynopsis()
    {
        $info = erpUserInfo();
        $website = $info['website'];
        $data = M('erp_homepage_synopsis')->where("website='$website'")->find();
        //SEO
        $this->assign('seo_title',$data['seotitle']);
        $this->assign('seo_description',$data['seodescription']);
        $this->assign('seo_keywords',$data['seokeywords']);
        //JS
        $this->assign('js_one',htmlspecialchars_decode($data['js_one']));
        $this->assign('js_two',htmlspecialchars_decode($data['js_two']));
        $this->assign('js_three',htmlspecialchars_decode($data['js_three']));
        //dump($data);
        //dump($data['seokeywords']);
        //content
        $this->assign('web_title',$data['title']);
        $this->assign('web_content',htmlspecialchars_decode($data['content']));
        $name = $data['static_html'];
        $this->buildHtml("$name",HTML_PATH . '/www.stanfordmaterials.com/',"Stanfordmaterials:webSynopsis");
        $this->display();
    }



    /**
     * 生成news静态界面
     */
    public function productStaticNewsHtml()
    {
        $info = erpUserInfo();
        $website = $info['website'];
        $data = M('stanfordmaterials_news')->where("website='$website'")->find();
        $this->assign('seo_title',$data['seotitle']);
        $this->assign('seo_description',$data['seodescription']);
        $this->assign('seo_keywords',$data['seokeywords']);
        //JS
        $this->assign('js_one',htmlspecialchars_decode($data['js_one']));
        $this->assign('js_two',htmlspecialchars_decode($data['js_two']));
        $this->assign('js_three',htmlspecialchars_decode($data['js_three']));
        //subNews
        $webid = intval($data['id']);
        $sub_news_data = M('stanfordmaterials_news_sub')->where("news_id=$webid")->select();
        $new_time = M('stanfordmaterials_news_sub')->field('update_time')->where("news_id=$webid")->order('update_time desc')->find();
        $new_time = date("l jS \of F Y h:i:s A",strtotime($new_time['update_time']));
        $this->assign('new_time',$new_time);
        foreach ($sub_news_data as $k=>$v){
            $sub_news_data[$k]['content'] = htmlspecialchars_decode($v['content']);
        }
        $this->assign('sub_news_info',$sub_news_data);
        $name = $data['static_html'];
        $this->buildHtml("$name",HTML_PATH . '/www.stanfordmaterials.com/',"Stanfordmaterials:news");
        $this->display('news');
    }









    /**
     * 生成Library静态界面
     */
    public function productStaticLibraryHtml()
    {
        $info = erpUserInfo();
        $website = $info['website'];
        $data = M('stanfordmaterials_library')->where("website='$website'")->find();
        $this->assign('seo_title',$data['seotitle']);
        $this->assign('seo_description',$data['seodescription']);
        $this->assign('seo_keywords',$data['seokeywords']);
        //JS
        $this->assign('js_one',htmlspecialchars_decode($data['js_one']));
        $this->assign('js_two',htmlspecialchars_decode($data['js_two']));
        $this->assign('js_three',htmlspecialchars_decode($data['js_three']));
        //subNews
        $webid = intval($data['id']);
        $sub_news_data = M('stanfordmaterials_library_sub')->where("news_id=$webid")->select();
        foreach ($sub_news_data as $k=>$v){
            $sub_news_data[$k]['content'] = htmlspecialchars_decode($v['content']);
        }
        $this->assign('sub_news_info',$sub_news_data);
        $name = $data['static_html'];
        $this->buildHtml("$name",HTML_PATH . '/www.stanfordmaterials.com/',"Stanfordmaterials:library");
        $this->display('library');
    }






}