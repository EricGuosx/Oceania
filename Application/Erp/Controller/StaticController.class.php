<?php
/**
 * StaticController.class.php
 * User: Eric
 * Date: 2018/4/4
 * Time: 15:29
 * Project: OceaniaErp
 */
namespace Erp\Controller;
use Think\Controller;
use Common\Helper\Category;
class StaticController extends ErpController
{
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * 网站首页
     */
    public function homePage()
    {
        $this->display();
    }

    /**
     * 公司简介
     */
    public function webSynopsis()
    {
        //拼接预览地址  http://five.com/WebHtml/www.stanfordmaterials.com/123.html

        $data = $_SERVER;
        $server_name = $data['SERVER_NAME'];
        $website = erpUserInfo()['website'];
        $static_html_data = M('erp_homepage_synopsis')->where("website='$website'")->find();
        $static_html = $static_html_data['static_html'];
        $preview = $server_name . '/WebHtml/' . $website . '/' . $static_html . '.html';
        $this->assign('preview',$preview);
        $this->display();
    }

    /**
     * 展示待生成静态界面的news列表
     */
    public function showNews()
    {
        $m = M('stanfordmaterials_news');
        $count = $m->count();
        $p = getpage($count,10);
        $list = M('stanfordmaterials_news')->field(true)->order('id DESC')->limit($p->firstRow, $p->listRows)->select();
        //previewUrl
        $data = $_SERVER;
        $server_name = $data['SERVER_NAME'];
        foreach ($list as $k=>$v){
            $list[$k]['preview_url'] = $server_name . '/WebHtml/' . $v['website'] . '/' . $v['static_html'] . '.html';
        }
        $this->assign('info', $list);
        $this->assign('page', $p->show());
        $this->display();
    }



    /**
     * 展示待生成静态界面的Library列表
     */
    public function showlibrary()
    {
        $m = M('stanfordmaterials_library');
        $count = $m->count();
        $p = getpage($count,10);
        $list = M('stanfordmaterials_library')->field(true)->order('id DESC')->limit($p->firstRow, $p->listRows)->select();
        //previewUrl
        $data = $_SERVER;
        $server_name = $data['SERVER_NAME'];
        foreach ($list as $k=>$v){
            $list[$k]['preview_url'] = $server_name . '/WebHtml/' . $v['website'] . '/' . $v['static_html'] . '.html';
        }
        $this->assign('info', $list);
        $this->assign('page', $p->show());
        //dump($list);
        $this->display();
    }




}